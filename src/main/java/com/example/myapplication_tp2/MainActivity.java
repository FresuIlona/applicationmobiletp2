package com.example.myapplication_tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        WineDbHelper dbHelper = new WineDbHelper(this);
        dbHelper.populate();
        final Cursor result = dbHelper.fetchAllWines();
        SQLiteDatabase db = dbHelper.getReadableDatabase();


        final ListView listview = (ListView) findViewById(R.id.listView);

        SimpleCursorAdapter adapter= new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, result, new String[] {WineDbHelper.COLUMN_NAME,
                WineDbHelper.COLUMN_WINE_REGION}, new int[] {android.R.id.text1, android.R.id.text2}, 0);
        listview.setAdapter(adapter);



        FloatingActionButton fab = findViewById(R.id.fab);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // Do something in response to the click
                String item = (String) listview.getItemAtPosition(position);
                Intent wineActivity = new Intent(MainActivity.this,WineActivity.class);
                //wineActivity.putExtra("wine", vin);
                startActivity(wineActivity);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
