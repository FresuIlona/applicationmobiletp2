package com.example.myapplication_tp2;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class WineActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Bundle intent = getIntent().getExtras();
        if(intent != null){

            TextView nom = findViewById(R.id.wineName);
            EditText region = findViewById(R.id.editWineRegion);
            EditText local = findViewById(R.id.editLoc);
            EditText climat = findViewById(R.id.editClimate);
            EditText plant = findViewById(R.id.editPlantedArea);
        }
    }
}
